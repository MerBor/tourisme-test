
import React from 'react';
import { Grid } from 'semantic-ui-react';
import './footer.scss';

const Footer = () => {
  return (
    <div>
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <div className="footer-label">
              <div className="footer-title">Déja membre?</div> <br />
              <div className="footer-subtitle">Votre code promo vous attend directement sur le site <a href="">en cliquant ici </a></div>
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
        <Grid.Column width={2}>
          </Grid.Column>
          <Grid.Column width={4}>
            <div className="footer-list-num">
              <div className="fooer-number"> 1 </div>
              <div className="footer-test">
                <span>REJOIGNER</span> <br />
                <span style={{ color: '#a52a2a' }}>EMIRATES | THE LIST</span>
              </div>
            </div>
          </Grid.Column>
          <Grid.Column width={4}>
            <div className="footer-list-num">
              <div className="fooer-number"> 2 </div>
              <div className="footer-test"> <span>RECEVEZ PAR EMAIL</span> <br />
                <span style={{ color: '#a52a2a' }}>VOTRE BON DE 150€ OFFERT</span><br />
                <span>à utliser dès 1000€ d'achat</span>
              </div>
            </div>
          </Grid.Column>

          <Grid.Column width={4}>

            <div className="footer-list-num">
              <div className="fooer-number">
                3
              </div>
              <div className="footer-test">
                <span>Réserver votre séjour</span> <br />
                <span style={{ color: '#a52a2a' }}>avant le 19 avril 2019</span>
              </div>
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row className="lsit">
          <Grid.Column>
          <div className="footer-title">
          <button className="footer-button">JE M'INSCRIS</button>
          </div>
             
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Grid className="footer-sticky">
        <Grid.Row>
          <Grid.Column>
            <div className="footer-bottom">
              <div className="footer-title-bottom">Rejoigner <b>Emirate | the list</b></div> <br />
              <div className="footer-subtitle-bottom">vol + hotel négocier jusqu'à -70% </div>
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </div>
  );
};

export default Footer;
