
import React from 'react';
import { Grid } from 'semantic-ui-react';
import './Header.scss';

const Header = () => {
  return (
      <Grid>
        <Grid.Row>
          <Grid.Column>
              <div className="header-title">Découvrer les offres du moment</div>
          </Grid.Column>
        </Grid.Row>
       
        <Grid.Row >
          <Grid.Column>
          <div className="header-subtitle">vol + hotel jusqu'à -70%</div>    
          </Grid.Column>
        </Grid.Row>
      </Grid>
  );
};

export default Header;
