
import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image } from 'semantic-ui-react';
import Slider from "react-slick";
import './Destination.scss';
const rating = (rate) => {
var ratings = "  ";
for(var i = 0; i< rate ; i++){
  ratings = ratings + "*"
}
return ratings;
}

// function LeftNavButton(props) {
//   const {className, style, onClick} = props
//   return (
//       <div
//           className="slick-arrow"
//           style={{...style, display: 'block'}}
//           onClick={onClick}
//       >
//           <img src={ARROW_left} alt="arrow_left"/>
//       </div>
//   );
// }
function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", background: "rgb(234, 84, 84)" }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "none" }}
      onClick={onClick}
    />
  );
}
const Destination = (props) => {
  const { data } = props;
  var settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />
  };
  return (
    <Card>
    <Slider {...settings}>
        <div>
        <Image src='CLOS_DU_LITTORAL.png' wrapped ui={false} />        
        </div>
        <div>
        <Image src='IMPIANA_RESORT_SAMUI.png' wrapped ui={false} />    
        </div>
        <div>
        <Image src='LAGUNA_BEACH.png' wrapped ui={false} />    
        </div>
      </Slider>
    <Card.Content>
      <Card.Header>{data.country} - <label className='destination-place'>{data.place}</label></Card.Header>
      <Card.Meta>
        <span className='date'>
        {data.label}
        <b>{rating(data.rating)}</b>
        </span>
      </Card.Meta>
      <Card.Description>
          <label className='label-premium'>{data.tags[0].label}</label>
          <label className='label-option'>{data.tags[1].label}</label> 

      </Card.Description>
    </Card.Content>
  </Card>
  );
};

Destination.propTypes = {
  data: PropTypes.object,
};

export default Destination;
