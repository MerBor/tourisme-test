import React from 'react';
import { Grid, Card } from 'semantic-ui-react';
import './ListDestination.scss';
import dest from '../../mocks/destinations';
import Destination from '../../components/destinationItem/destination';

class ListDestination extends React.Component {
  constructor(props) {
    super(props);
    this.state = {destination: 'destination' };
  }

  onChange = (event) =>{
    this.setState({destination: event.target.value});
  }
  render() {
    const { destination } = this.state;
    const filteredCountry = destination === 'destination' ?
    dest.destinations : dest.destinations.filter(item => item.country === destination);
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <div className="liste-destination">
              <select className="styled-select" onChange={this.onChange} value={this.state.value}>
                <option value="destination"> DESTINATION</option>
                <option value="Emirates Arabes Unis"> Emirates Arabes Unis</option>
                <option value="Maurice"> Maurice</option>
                <option value="Sri Lanka"> Sri Lanka</option>
              </select>
            </div>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Card.Group className="users_list">
              { 
                filteredCountry.map((item) => (
                <Destination
                  key={item.label}
                  data={item}
                />
              ))
            }
            </Card.Group>
            <span className="destination-subtitle">comment bénéficier de l'offre ?</span>
            <span className="destination-subtitle dest">150% de réduction* dès 1000€ d'achat</span>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default ListDestination;
