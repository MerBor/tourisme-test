import React from 'react';
import ListDestination from './containers/destinationList/ListDestinations';
import Footer from './components/footer/footer';
import Header from './components/header/Header';

function App() {
  return (
    <div className="App">
     <Header />
     <ListDestination />
     <Footer />
    </div>
  );
}

export default App;
